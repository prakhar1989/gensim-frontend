var React = require('react');
var ReactDOM = require('react-dom');
var Select = require('react-select');
var request = require('superagent');

var Email = React.createClass({
    render: function() {
        return <div>
            <hr />
            <h1> {this.props.id} </h1>
            <p> {this.props.title} </p>
        </div>
    }
});

var App = React.createClass({
    getInitialState() {
        return {
            results: [],
            query: "Enter query",
            email: null
        }
    },
    getOptions(input, callback) {
        if (input.length < 3) return ;
        request.get('/words/' + input)
            .end((err, res) => callback(null, { options: res.body.words }));
    },
    logChange(val) {
        var self = this;
        request.get('/find/' + val).
            end((err, res) => {
                self.setState({results: res.body.results})
            });
    },
    viewEmail(emailId) {
        this.setState({email: this.state.results[emailId]})
    },
    render: function() {
        let {query, email} = this.state;
        var renderedResults = this.state.results.map(function (e, index) {
            return <li key={index}>{e.title} | <button onClick={this.viewEmail.bind(null, index)}>View Email</button></li>
        }.bind(this));
        return <div>
            <Select
                name="form-field-name"
                value={query}
                asyncOptions={this.getOptions}
                onChange={this.logChange}
            />
            <ul> {renderedResults } </ul>
            { email !== null ?  <Email id={email.id} title={email.title} /> : null }
        </div>
    }
});

ReactDOM.render(<App />, document.getElementById('app'));
