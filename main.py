from flask import Flask, jsonify, render_template
import json

app = Flask(__name__)
words = None

with open('/usr/share/dict/words') as f:
    words = filter(lambda w: len(w) > 4, f.read().split())

@app.route('/words/<pre>')
def spellcheck(pre):
    filterWords = [w for w in words if w.startswith(pre)]
    return jsonify({"words": [{"value": w, "label": w} for w in filterWords] })

# returns a json with emails that contain that word
@app.route('/find/<word>')
def getEmails(word):
    return jsonify({"results": [
        {"id": 10, "title": "hello world " + word},
        {"id": 60, "title": "good world " + word},
        {"id": 30, "title": "lorem world " + word},
        {"id": 50, "title": "hello ipsum " + word},
        {"id": 20, "title": "last email " + word}
    ]})

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)
